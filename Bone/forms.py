from django import forms

from .models import Bone

class BoneCreateForm(forms.ModelForm):
    class Meta:
        model = Bone
        fields = [
        'name',
        'more_info',
        'keywords'
        ]
