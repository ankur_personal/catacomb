from django.conf import settings
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.urls import reverse
from taggit.managers import TaggableManager
# Create your models here.

User = settings.AUTH_USER_MODEL

class Bone(models.Model):
    # id          = models.IntegerField(primary_key=True)
    name        = models.CharField(max_length=120)
    more_info   = models.TextField()
    parent      = models.IntegerField(blank=True, null=True)
    keywords    = TaggableManager()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("bone-list-child", kwargs={"id": str(self.id)})

    def get_address(self):
        is_root = False
        address = []
        current = self
        while True:
            if is_root:
                break
            if current.parent:
                address.append(current.name)
                current = Bone.objects.get(id = current.parent)

            else:
                address.append(current.name)
                address.append("Root")
                is_root = True


        address_string = ""
        address.reverse()
        del address[-1]
        for bone in address:
            address_string = address_string + " - " + bone

        return address_string
