from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.shortcuts import get_list_or_404, get_object_or_404
from django.conf import settings
import pprint

pp = pprint.PrettyPrinter(indent=4)

from .models import *
from .forms import *
from taggit.models import Tag

# Create your views here.
User = settings.AUTH_USER_MODEL


class BoneListView(ListView):
    model = Bone

    def get_queryset(self):
        if self.kwargs.get('id'):
            parent = self.kwargs.get('id')
        else:
            parent = 0
        queryset = Bone.objects.filter(parent=parent)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.kwargs.get('id'):
            new_context_entry = Bone.objects.get(id=self.kwargs.get('id'))
            context["parent_obj"] = new_context_entry
            tags = Tag.objects.filter(taggit_taggeditem_items__object_id=new_context_entry.id)
            context["tags"] = tags

        return context

class BoneDetailView(DetailView):
    model = Bone

class BoneCreateView(CreateView):
    form_class = BoneCreateForm
    template_name = 'Bone/create_bone.html'
    queryset = Bone.objects.all()

    def form_valid(self, form):
        newbone = form.save(commit=False)
        form.instance.parent = self.kwargs.get('id')
        newbone.save()
        form.save_m2m()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('bone-list')


class BoneSearchView(ListView):
    model = Bone
    template_name = 'Bone/bone_search.html'
    def get_queryset(self):
        q = self.request.GET.get('q')
        results = Bone.objects.filter(keywords__name=q)
        return results
